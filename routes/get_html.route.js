const router = require("express").Router();
const axios = require("axios");

router.get("/", (req, res, next) => {
  const cityName = req?.query?.city || process.env.default_city || 'brisbane';
  const countryCode = req?.query?.countrycode || process.env.default_countrycode || 'in';
  const weather_url = `${process.env.weather_api_uri}?city=${cityName}&key=${process.env.weather_api_access_key}`;
  const news_url = `${process.env.news_api_uri}?country=${countryCode}&apiKey=${process.env.news_api_key}`;
  const reqOne = axios.get(news_url);
  const reqTwo = axios.get(weather_url);
  const default_image  = process.env.default_image_uri;
axios.all([reqOne, reqTwo]).then(axios.spread((...responses) => {
    const newsData = responses[0]?.data?.articles;
    const weatherData = responses[1]?.data?.data;
    res.render('weather_news', {weatherData,newsData, default_image});

  })).catch(errors => {
    res.render('404', {status : 404, msg :'NOT FOUND' });
  })
  
});

module.exports = router;

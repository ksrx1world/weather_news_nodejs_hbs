const express = require('express');
const cors = require('cors');
const expressHbs = require('express-handlebars');
const path = require('path');
require('dotenv').config();


const app = express()
const port = 4300
app.use(cors());
app.use(express.json());
app.engine('.hbs', expressHbs.engine({ extname: '.hbs', defaultLayout: ""}));
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, './views/layouts'));
app.use(express.static("./views/css"));



const html= require('./routes/get_html.route') 
app.use("/", html);

app.use((req,res, next) => {
    const error = new Error('OOPS PAGE NOT FOUND');
    error.status = 404;
    next(error);
})

app.use((error, req,res, next) => {
    let error_Status = error.status;
    console.log({status : error_Status, msg :error.message});
    res.render('404', {status : error_Status, msg :error.message });

    // res.status(error.status || 500);
    // res.json({
    //     error: {
    //         message:error.message
    //     }
    // })
})

app.listen(port, () => console.log(`server is connected`))